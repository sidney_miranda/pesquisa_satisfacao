const express = require('express');
const path = require('path');
const fs = require('fs');
const cors = require('cors');
const moment = require('moment');

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'view')));  


app.get('/', (_, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.post('/pesquisa', (req, res) => {
    try {
        moment.locale('pt-br');
        const dados = `${req.body.pesquisa01}, ${req.body.pesquisa02}, ${req.body.pesquisa03}, ${req.body.pesquisa04}, ${req.body.pesquisa05}, ${req.body.pesquisa06}, ${moment().format('LLL')}`;
        fs.appendFile('pesquisa.csv', `${dados};\n`, (err) => {
            if (err)
                throw err
            return res.status(500).send({ status: '500', message: 'Pesquisa registrada com sucesso!' });
        });
    } catch (err) {
        return res.status(500).send({ status: '500', message: err.message });
    }
});


app.listen(3000, () => {
    console.log('server is up...');
});

