document.querySelector('#enviar').addEventListener('click', async () => {
    const dados = {
        pesquisa01: document.querySelector('input[name="pesquisa01"]').value,
        pesquisa02: document.querySelector('input[name="pesquisa02"]').value,
        pesquisa03: document.querySelector('input[name="pesquisa03"]').value,
        pesquisa04: document.querySelector('input[name="pesquisa04"]').value,
        pesquisa05: document.querySelector('input[name="pesquisa05"]').value,
        pesquisa06: document.querySelector('input[name="pesquisa06"]').value
    }

    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dados)
    }

    const response = await fetch('http://localhost:3000/pesquisa', options);
    reset();
    const message = await response.json();

    showMessage(message);

});

function reset() {
    document.querySelector('#form').reset();
}

function showMessage(message) {
    window.location.hash = '#form';

    const msg = document.querySelector('.msg');
    msg.innerHTML = "";

    const div = document.createElement('div');

    if (message.status == '500') {
        div.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path
                d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
        </symbol>
    </svg>
    <div class="alert alert-danger d-flex justify-content-center col-5 position-absolute top-0 end-0 fade show" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
            <use xlink:href="#exclamation-triangle-fill" />
        </svg>
        <div>
            ${JSON.stringify(message)}
        </div>
    </div>`;
    } else {
        div.innerHTML = `
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </symbol>
        </svg>
        <div class="alert alert-success d-flex justify-content-center col-5 position-absolute top-0 end-0 fade show" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
            <div>
                ${JSON.stringify(message)}
            </div>
        </div>`
    }

    msg.append(div);

    setTimeout(() => {
        msg.classList.add('transform');

        setTimeout(() => {
            window.location.replace('http://localhost:3000/index.html');
            msg.remove('class', 'fade');
            msg.remove('class', 'show');
        }, 1000);
    }, 2000);
}